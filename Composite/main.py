import unittest
from abc import ABC
from collections.abc import Iterable


class ConnectValues(ABC, Iterable):
    @property
    def sum(self):
        count = 0
        for parameter in self:
            for value in parameter:
                count += value
        return count


class SingleValue(ConnectValues):
    def __init__(self, value):
        self.value = value

    def __iter__(self):
        yield self.value


class ManyValues(list, ConnectValues):
    pass


class FirstTestSuite(unittest.TestCase):
    def test(self):
        single_value = SingleValue(11)
        other_values = ManyValues()
        other_values.append(22)
        other_values.append(33)

        all_values = ManyValues()
        all_values.append(single_value)
        all_values.append(other_values)

        self.assertEqual(all_values.sum, 66)
