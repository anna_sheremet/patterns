class Sentence:
    def __init__(self, plain_text):
        self.words = plain_text.split(" ")
        self.collection = {}

    def __getitem__(self, item):
        status = self.WordStatus()
        self.collection[item] = status
        return self.collection[item]

    class WordStatus:
        def __init__(self, capitalize=False):
            self.capitalize = capitalize

    def __str__(self):
        result = []
        for idx in range(len(self.words)):
            word = self.words[idx]
            if idx in self.collection and self.collection[idx].capitalize:
                word = word.upper()
            result.append(word)
        return " ".join(result)


if __name__ == "__main__":
    text = "hello world"
    sentence = Sentence(text)
    sentence[1].capitalize = True
    print(sentence)  # writes "hello WORLD"
