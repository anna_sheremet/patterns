import copy


class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return f"{self.x}, {self.y}"


class Line:
    def __init__(self, start=Point(), end=Point()):
        self.start = start
        self.end = end

    def __str__(self):
        return f"This line gets start at {self.start} and finish at {self.end}"

    def deep_copy(self):
        return copy.deepcopy(self)


first_line = Line(Point(1, 2), Point(3, 4))
print(first_line)
second_line = first_line.deep_copy()
print(second_line)