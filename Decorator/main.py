class Circle:
    def __init__(self, radius):
        self.radius = radius

    def resize(self, factor):
        self.radius *= factor

    def __str__(self):
        return f"A circle of radius {self.radius}"


class Square:
    def __init__(self, side):
        self.side = side

    def __str__(self):
        return f"A square of length {self.side}"


class ColoredShape:
    def __init__(self, shape, color):
        self.color = color
        self.shape = shape

    def resize(self, factor):
        # Method resize belongs only to circle shape
        resize = getattr(self.shape, "resize", None)
        if callable(resize):
            self.shape.resize(factor)
        # todo
        # note that a Square doesn't have resize()

    def __str__(self):
        return f"This {self.shape} has color {self.color}"
