from abc import ABC
from enum import Enum, auto


class HotDrinkBuilder:
    def __init__(self):
        self.drink = None

    def get(self):
        return self.drink


class CoffeeBuilder(HotDrinkBuilder):
    pass


class TeaBuilder(HotDrinkBuilder):
    def __init__(self):
        super().__init__()
        self.drink = Tea()

    def with_sugar(self, yes=1):
        self.drink.sugar = yes
        return self


class HotDrink(ABC):
    def consume(self):
        pass


class Tea(HotDrink):
    def __init__(self):
        self.sugar = 0

    def consume(self):
        print('This tea is nice but I\'d prefer it with milk')


class Coffee(HotDrink):
    def consume(self):
        print('This coffee is delicious')


class HotDrinkFactory(ABC):
    def prepare(self, amount):
        pass


class TeaFactory(HotDrinkFactory):
    def __init__(self):
        self.builder = TeaBuilder()

    def prepare(self, amount):
        self.builder.with_sugar(1).get()
        print(f'Put in tea bag, boil water, pour {amount}ml, add {self.builder.drink.sugar} spoon of sugar, enjoy!')
        return Tea()


class CoffeeFactory(HotDrinkFactory):
    def prepare(self, amount):
        print(f'Grind some beans, boil water, pour {amount}ml, enjoy!')
        return Coffee()


class HotDrinkMachine:
    class AvailableDrink(Enum):  # violates OCP
        COFFEE = auto()
        TEA = auto()

    factories = []
    initialized = False

    def __init__(self):
        if not self.initialized:
            self.initialized = True
            for d in self.AvailableDrink:
                name = d.name[0] + d.name[1:].lower()
                factory_name = name + 'Factory'
                factory_instance = eval(factory_name)()
                self.factories.append((name, factory_instance))

    def make_drink(self):
        print('Available drinks:')
        for f in self.factories:
            print(f[0])

        s = input(f'Please pick drink (0-{len(self.factories) - 1}): ')
        idx = int(s)
        s = input(f'Specify amount in ml: ')
        amount = int(s)
        return self.factories[idx][1].prepare(amount)


if __name__ == '__main__':
    hdm = HotDrinkMachine()
    drink = hdm.make_drink().consume()
