class Person:
    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __str__(self):
        return f"Id: {self.id}; name: {self.name}"


class PersonFactory:
    counter = 0

    def __init__(self):
        self.id = PersonFactory.counter
        PersonFactory.counter += 1

    def create_person(self, name):
        return Person(self.id, name)


if __name__ == '__main__':
    p1 = PersonFactory()
    person1 = p1.create_person("Jane")
    p2 = PersonFactory()
    person2 = p2.create_person("John")
    p3 = PersonFactory()
    person3 = p3.create_person("Clare")
    print(person1, person2, person3)
