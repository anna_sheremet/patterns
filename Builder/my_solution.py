class Field:
    def __init__(self, name, value):
        self.value = value
        self.name = name

    def __str__(self):
        return "self.%s = %s" % (self.name, self.value)


class Code:
    indent_size = 2
    fields = []

    def __init__(self, name):
        self.name = name

    def add_field(self, type, text):
        self.fields.append(Field(type, text))
        return self

    def __str__(self, indent=0):
        lines = ['class %s:' % self.name]
        if self.fields:
            i = " " * self.indent_size
            indent = 1
            lines.append('%sdef __init__(self):' %i)
            for field in self.fields:
                i = " " * ((indent + 1) * self.indent_size)
                lines.append("%s%s" % (i, field))
        else:
            lines.append(f'{" " * self.indent_size}pass')
        return "\n".join(lines)


class CodeBuilder:
    def __init__(self, root_name):
        self.__class = Code(root_name)

    def add_field(self, type, name):
        return self.__class.add_field(type, name)

    def __str__(self, indent=0):
        return self.__class.__str__()


cb = CodeBuilder('Foo')
print(cb)
