class Person:
    def __init__(self, age):
        self.age = age

    def drink(self):
        return 'drinking'

    def drive(self):
        return 'driving'

    def drink_and_drive(self):
        return 'driving while drunk'


class ResponsiblePerson:
    def __init__(self, person):
        self.person = person
        self.is_drank = False

    def drink(self):
        if self.person.age >= 18:
            self.is_drank = True
            return self.person.drink()
        else:
            return "Person is too young to drink"

    def drive(self):
        if self.is_drank:
            exit("dead")

        if self.person.age >= 16:
            self.is_drank = False
            return self.person.drive()
        else:
            return "Person is too young to drive"


if __name__ == '__main__':
    person = Person(18)
    resp_person = ResponsiblePerson(person)
    print(resp_person.drive(), "\n", resp_person.drink(), "\n", resp_person.drive())