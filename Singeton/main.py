def is_singleton(factory):
    checked_parameter = factory()
    second_execution = factory()
    return checked_parameter is second_execution


class SomeFactory():
    def __init__(self):
        self.name = "new Class"

    def create_note(self):
        return "Here is %s" % self.name


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, *kwargs)
        return cls._instances[cls]


class SingletonFactory(metaclass=Singleton):
    def __init__(self):
        self.name = "Single"

    def create_note(self):
        return "Here is %s" % self.name


print(is_singleton(SingletonFactory))
print(is_singleton(SomeFactory))