from abc import ABC


class Shape:
    def __init__(self, renderer, name):
        self.name = name
        self.renderer = renderer

    def __str__(self):
        return f"Drawing {self.name} as {self.renderer.type}"


class Triangle(Shape):
    def __init__(self, renderer):
        super().__init__(renderer, "Triangle")


class Square(Shape):
    def __init__(self, renderer):
        super().__init__(renderer, "Square")


class Renderer(ABC):
    @property
    def type(self):
        return None


class VectorRenderer(Renderer):
    @property
    def type(self):
        return 'lines'


class RasterRenderer(Renderer):
    @property
    def type(self):
        return 'pixels'


print(str(Triangle(RasterRenderer())))
