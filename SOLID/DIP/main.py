# Dependency Inversion Principle
# Depend on abstraction not on low-level moduls (depend on interface not realisation)
from abc import abstractmethod
from enum import Enum


class Relationship(Enum):
    PARENT = 0
    CHILD = 1
    SIBLING = 2


class Person:
    def __init__(self, name):
        self.name = name


class RelationshipBrowser:
    @abstractmethod
    def find_all_children_of(self, name):
        pass


class Relationships(RelationshipBrowser):  # low-level module
    def __init__(self):
        self.relations = []

    def add_parent_and_child(self, parent, child):
        self.relations.append(
            (parent, Relationship.PARENT, child),
        )

    # method below prevents another class dependency of low-level storage
    def find_all_children_of(self, name):
        for r in self.relations:
            if r[0].name == name and r[1] == Relationship.PARENT:
                yield r[2].name


# now class Research doesn't care if Relationships attempts to change method of storage
class Research:  # high-level
    def __init__(self, browser, name):
        for p in browser.find_all_children_of(name):
            print(f'John has a child called {p}')
    # old wrong code:
    # def __init__(self, relatives):
    #     relations = relatives.relations
    #     for r in relations:
    #         if r[0].name == 'John' and r[1] == Relationship.PARENT:
    #             print(f"John has a child called {r[2].name}.")


myparent = Person('John')
child1 = Person('Matt')
child2 = Person('Sue')

relationships = Relationships()
relationships.add_parent_and_child(parent=myparent, child=child1)
relationships.add_parent_and_child(parent=myparent, child=child2)

Research(relationships, 'John')
