# Liskov Substitution Principle
class Rectangle:
    def __init__(self, width, height):
        self._width = width
        self._height = height

    @property
    def area(self):
        return self._height * self._width

    def __str__(self):
        return f"width: {self.width}, height: {self.height}"

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        self._width = value

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value


class Square(Rectangle):
    def __init__(self, size):
        Rectangle.__init__(self, size, size)

# methods below disrupts a Liskov substitution principle
    @Rectangle.width.setter
    def width(self, value):
        self._width = self._height = value

    @Rectangle.height.setter
    def height(self, value):
        self._width = self._height = value


# function below that works only with class Rectangle and doesn't with his heirs. This disrupts a Liskov substitution
# principle
def sides_usage(rc):
    w = rc.width
    rc.height = 10
    expected = int(w * 10)
    print(f"Expected area is {expected}, we have got {rc.area}")


rc = Rectangle(2, 3)
sides_usage(rc)
sq = Square(5)
sides_usage(sq)
