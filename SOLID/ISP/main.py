# Interface segregation Principle
# Separate functionality, not whole in one basic class
from abc import abstractmethod, ABC


class Printer:
    @abstractmethod
    def print(self):
        pass


class Scanner:
    @abstractmethod
    def scan(self):
        pass


class Fax:
    @abstractmethod
    def fax(self):
        pass


class MyPrinter(Printer):
    def __init__(self, document):
        self.document = document

    def print(self):
        print(f"Printed: {document}")


class PhotoCopier(Printer, Scanner):
    def print(self):
        print(f"Printed: {document}")

    def scan(self):
        print(f"Scanned: {document}")


# if we need multifunctional machine, we allowed to add interface using abstract classes above and
# then use it in specific classes
class MultiFunctionalDevice(Printer, Scanner, Fax):
    @abstractmethod
    def fax(self):
        pass

    @abstractmethod
    def scan(self):
        pass

    @abstractmethod
    def print(self):
        pass


class MultiFunctionalMachine(MultiFunctionalDevice):
    def __init__(self, document):
        self.document = document

    def print(self):
        print(f"Printed: {document}")

    def scan(self):
        print(f"Scanned: {document}")

    def fax(self):
        print(f"Fax sent: {document}")


document = "my document"
mfm = MultiFunctionalMachine(document)
mfm.print()
