# Single responsibility principle
class Journal:
    def __init__(self):
        self.entries = []
        self.count = 0

    def add_entry(self, text):
        self.count += 1
        self.entries.append(f"{self.count}: {text}")

    def remove_entry(self, pos):
        del self.entries[pos]

    def __str__(self):
        return '\n'.join(self.entries)


class PersistenceManager:
    @staticmethod
    def save_to_file(journal, filename):
        file_editor = open(filename, "w")
        file_editor.write(str(journal))
        file_editor.close()


j = Journal()
j.add_entry('I cried today, because I ate a bug')
j.add_entry('BTW I ate a bug in the morning')
print(f"Journal entries:\n{j}")

file = r'/home/xone/PycharmProjects/Python/Patterns/SOLID/SRP/journal.txt'
PersistenceManager.save_to_file(j, file)

with open(file) as fh:
    print(fh.read())
    fh.close()
